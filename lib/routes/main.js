/* global $ */
/* eslint-disable no-multi-str */

module.exports = {
  login: function * (next) {
    try {
      // Log them in
    } catch (err) {
      console.log(err);
    }
  },
  getData: function * (next) {
    try {
      var data = yield $.db.read();
      var self = this;
      self.body = {
        data: data,
        message: 'Successful return of data'
      };
      self.response.status = 200;
      console.log(self);
    } catch (err) {
      console.log(err);
    }
  },
  getUserData: function * (next) {
    var data = yield $.db.read();
    var self = this;
    data = data.users[0];
    self.body = data;
  },
  postData: function * (next) {
    try {
      var self = this;
      console.log(self.request.body);
      var data = yield $.db.read();
      console.log('read');
      data.catalogs.unshift(self.request.body);
      console.log('altered');
      $.db.write(data);
      console.log('written');
      self.body = {
        message: 'Successful Post to API'
      };
      console.log(self);
    } catch (err) {
      console.log(err);
      self.body = {
        message: 'Unsuccessful Post to API'
      };
    }
  },
  postNewCatalog: function * (next) {
    try {
      var self = this;
      var payload = self.request.body;
      var data = yield $.db.read();
      var catalog = {
        catalogName: payload.name,
        songs: []
      };
      data.users[0].catalogs.push(catalog);
      $.db.write(data);
      self.body = {
        message: 'Successful Post to API'
      };
      console.log(self);
    } catch (e) {
      console.log(e);
    }
  },
  postNewSong: function * (next) {
    try {
      var self = this;
      var payload = self.request.body.data;
      console.log('PAYLOAD::: ', payload);
      var data = yield $.db.read();
      var song = {
        song: payload,
      };
      data.songs.push(song);
      $.db.write(data);
      self.body = {
        message: 'Successful Post to API'
      };
      console.log(self);
    } catch (e) {
      console.log(e);
    }
  }
};
