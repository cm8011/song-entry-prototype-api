/* global $ */

var delay = require('koa-delay');

$.koa.use(delay(300, 50));
