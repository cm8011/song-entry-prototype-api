/* global $ */
$.koa.use(function *(next) {
  var start = new Date();
  yield next;
  var ms = new Date() - start;
  this.set('X-Response-Time', ms + 'ms');
  this.set('access-control-allow-origin', $.config.originAccess);
});
