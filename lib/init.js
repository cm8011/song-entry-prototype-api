module.exports = function () {
  return new Promise(function (resolve, reject) {
    try {
      var $ = GLOBAL.$ = GLOBAL.$ || {};
      $.ENV = process.env.ENV || 'dev';
      $.state = {};
      var dir = __dirname;
      require(dir + '/require')()
      .then(require(dir + '/load-config'))
      .then(resolve)
      .catch(reject);
    } catch (err) {
      reject(err);
    }
  });
};
