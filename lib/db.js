/* global $ */

var tempDir = __dirname + '/../db/temp';
var dataModel = require('./../db/dataModel.js');

module.exports = {
  init: function () {
    try {
      console.log('writing initialDB');
      this.write(dataModel);
      console.log('success');
    } catch (e) {
      $.handleError(e);
    }
  },
  read: function () {
    return new Promise(function (resolve, reject) {
      $.fs.readFile(tempDir + '/data.json', function (err, data) {
        if (err) { reject(err); }
        resolve(JSON.parse(data));
      });
    });
  },
  write: function (data) {
    return new Promise(function (resolve, reject) {
      var parsedData = JSON.stringify(data, null, 2);
      $.fs.writeFile(tempDir + '/data.json', parsedData, 'utf-8', function (err, data) {
        if (err) { console.log(err); }
        resolve;
      });
    });
  }
};

// function writeDB (data) {
//   return new Promise(function (resolve, reject) {
//     var parsedData = JSON.stringify(data, null, 2);
//     $.fs.writeFile(tempDir + '/data.json', parsedData, 'utf-8', function (err, data) {
//       if (err) { console.log(err); }
//       resolve;
//     });
//   });
// }

// function readDB () {
//   return new Promise(function (resolve, reject) {
//     $.fs.readFile(tempDir + '/data.json', function (err, data) {
//       if (err) { reject(err); }
//       resolve(JSON.parse(data));
//     });
//   });
// }
