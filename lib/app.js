/* global $ */

require(__dirname + '/middleware/simulateLoadTime.js');
require(__dirname + '/middleware/headers.js');

$.koa.use($.cors());
$.koa.use($.koaRouter.routes());
$.koa.use($.koaBody);

// Get
$.koaRouter.get('/auth', $.routes.login);
$.koaRouter.get('/getData', $.routes.getData);
$.koaRouter.get('/getUserData', $.routes.getUserData);

// Post
$.koaRouter.post('/postData', $.koaBody, $.routes.postData);
$.koaRouter.post('/postNewCatalog', $.koaBody, $.routes.postNewCatalog);
$.koaRouter.post('/postNewSong', $.koaBody, $.routes.postNewSong);

var port = process.env.PORT || 3010;
$.koa.listen(port);

console.log($.config);

console.log('--- API up on port ' + port + ' ---');
