/* global $ */
module.exports = function () {
  return new Promise(function (resolve, reject) {
    try {
      $._ = require('lodash');
      $.assert = require('assert');
      $.cors = require('kcors');
      $.db = require('./db.js');
      $.fs = require('fs-extra');
      $.koa = require('koa')();
      $.koaBody = require('koa-body')();
      $.koaRouter = require('koa-router')();
      $.moment = require('moment');
      $.request = require('request');
      $.routes = require('./routes/main.js');
      $.util = require('util');
      $.db.init();
      resolve();
    } catch (err) {
      reject(err);
    }
  });
};
