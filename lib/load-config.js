/* global $ */

module.exports = function () {
  return new Promise(function (resolve, reject) {
    $.fs.readFile(
      __dirname + '/../' + [$.ENV] + '.json',
      'utf8',
      function (err, data) {
        if (err) reject(err);
        $.config = JSON.parse(data);
        resolve();
      });
  });
};
