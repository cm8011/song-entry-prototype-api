/* global $ */
'use strict';

require('strict-version');

var dir = __dirname + '/lib';
require(dir + '/init')()
  .then(function () {
    require(dir + '/app');
  })
  .catch($.handleError);
