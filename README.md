# Song Entry Prototype API

### To run project
- If you don't have it, install [nodejs](https://nodejs.org/en/download/)
- You may need to update npm in if you are in a windows environment (npm -v > 3.0.0)

##### Serve API
```bash
npm install
node server/index.js
```

- To deploy set production.json {"originAccess": "[https://[my-heroku-app]"}